
/*
 * app
 * https://github.com/Dantas/app
 *
 * Copyright (c) 2015 Dantas
 * Licensed under the MIT license.
 */

'use strict';

// import

var express = require('express'),
    app = express(),
    port = 8000

// configure

app.use(express['static'](__dirname + '/../www'))


// null redirect to index
app.all('', function(req, res) {
    res.sendFile(__dirname + '/../www/index.html');
})


// catch
app.all('*', function(req, res) {
    res.sendFile(__dirname + '/errors/404.html')
})

// bind

app.listen(port)

console.log('Servidor iniciando na porta ' + port + '.');
